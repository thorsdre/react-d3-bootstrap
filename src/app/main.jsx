/**
 * In this file, we create a React component
 * which incorporates components providedby material-ui.
 */

import React from 'react';
import rd3 from 'rd3';
import barData from './data/barData';
import lineData from './data/lineData';
import areaData from './data/areaData';
import scatterData from './data/scatterData';
import pieData from './data/pieData';

const BarChart = rd3.BarChart;
const LineChart = rd3.LineChart;
const ScatterChart = rd3.ScatterChart;
const AreaChart = rd3.AreaChart;
const PieChart = rd3.PieChart;

const Main = React.createClass({
  render(){
    return(
      <div>
        <h2>D3 React</h2>
        <div className="row">
          <div className="col-md-6">
            <BarChart
              // legend={true}
              data={barData}
              width={500}
              height={300}
              title="Bar Chart"
              yAxisLabel="y Axis Label"
              xAxisLabel="x Axis Label"
              gridHorizontal={true}
            />
          </div>
          <div className="col-md-6">
            <LineChart
              // legend={true}
              data={lineData}
              width={500}
              height={300}
              title="Line Chart"
              yAxisLabel="y Axis Label"
              xAxisLabel="x Axis Label"
              gridHorizontal={true}
            />
          </div>
          <div className="col-md-6">
            <ScatterChart
              data={scatterData}
              width={500}
              height={300}
              yHideOrigin={true}
              title="Scatter Chart"
              yAxisLabel="y Axis Label"
              xAxisLabel="x Axis Label"
              gridHorizontal={true}
              gridVertical={true}
            />
          </div>
          <div className="col-md-6">
            <AreaChart
              data={areaData}
              width={500}
              height={300}
              xAxisTickInterval={{unit: 'year', interval: 10}}
              title="Area Chart"
              yAxisLabel="y Axis Label"
              xAxisLabel="x Axis Label"
              gridHorizontal={true}
            />
          </div>
          <div className="col-md-12">
            <PieChart
              data={pieData}
              width={400}
              height={400}
              radius={100}
              innerRadius={20}
              sectorBorderColor="white"
              title="Pie Chart"
            />
          </div>
        </div>
      </div>
    );
  },
});

export default Main;