import React from 'react';
import ReactDOM from 'react-dom';

const Axis = React.createClass({

  propTypes: {
    h:React.PropTypes.number,
    axis:React.PropTypes.func,
    axisType:React.PropTypes.oneOf(['x','y']),
  },

  componentDidUpdate: function () { this.renderAxis(); },
  componentDidMount: function () { this.renderAxis(); },

  renderAxis: function () {
    const node = ReactDOM.findDOMNode(this);
    d3.select(node).call(this.props.axis);
  },

  render(){
    let translate = "translate(0,"+(this.props.h)+")";
    return (
      <g className="axis" transform={this.props.axisType === 'x' ? translate:""} ></g>
    );
  },
});

export default Axis;