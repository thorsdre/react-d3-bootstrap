import React from 'react';

const Dots = React.createClass({

  propTypes: {
    data:React.PropTypes.array,
    x:React.PropTypes.func,
    y:React.PropTypes.func,
    fill:React.PropTypes.string,
    stroke:React.PropTypes.string,
    strokeWidth:React.PropTypes.number,
    radius:React.PropTypes.number,
  },

  getDefaultProps() {
    return {
      fill: "#7dc7f4",
      strokeWidth: 5,
      radius: 7,
    };
  },

  getInitialState() {
    return {
      stroke:this.props.stroke,
    };
  },

  render(){
    const _self = this;
    //remove last & first point
    let data = this.props.data.splice(1);
    data.pop();

    let circles = data.map(function(d,i) {

      return (
        <circle
          className="dot"
          r={_self.props.radius}
          cx={_self.props.x(d.date)}
          cy={_self.props.y(d.count)}
          fill={_self.props.fill}
          stroke={_self.props.stroke}
          strokeWidth={_self.props.strokeWidth}
          key={i}
          onMouseOver={_self.props.showToolTip}
          onMouseOut={_self.props.hideToolTip}
          data-key={d3.time.format("%b %e")(d.date)}
          data-value={d.count} />
      )
    });

    return(
      <g>{circles}</g>
    );
  },
});

export default Dots;
