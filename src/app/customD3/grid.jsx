import React from 'react';
import ReactDOM from 'react-dom';

const Grid = React.createClass({

  propTypes: {
    h:React.PropTypes.number,
    grid:React.PropTypes.func,
    gridType:React.PropTypes.oneOf(['x','y']),
  },

  componentDidUpdate: function () { this.renderGrid(); },
  componentDidMount: function () { this.renderGrid(); },

  renderGrid: function () {
    const node = ReactDOM.findDOMNode(this);
    d3.select(node).call(this.props.grid);
  },

  render(){
    let translate = "translate(0,"+(this.props.h)+")";
    return (
      <g className="y-grid" stroke="#ccc" strokeWidth="1px" transform={this.props.gridType === 'x' ? translate : ""}></g>
    );
  },
});

export default Grid;