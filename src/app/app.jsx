import React from 'react';
import ReactDOM from 'react-dom';
import d3 from 'd3';
import rd3 from 'rd3';
import injectTapEventPlugin from 'react-tap-event-plugin';
import D3demo from './d3demo'; // Our custom react component
import Main from './main'; // Our custom react component
import Bootstrap from './bootstrap'; // Our custom react component

//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

// Render the header app react component into the app div.
// For more details see: https://facebook.github.io/react/docs/top-level-api.html#react.render
ReactDOM.render(<D3demo dataUrl="data/data.json" />, document.getElementById('d3demo'));
ReactDOM.render(<D3demo dataUrl="data/data2.json" />, document.getElementById('d3demo2'));
ReactDOM.render(<Main />, document.getElementById('main'));
ReactDOM.render(<Bootstrap />, document.getElementById('bootstrap'));

// D3 Renderer
d3.select("body").append("h1").text("This was rendered by D3!");
