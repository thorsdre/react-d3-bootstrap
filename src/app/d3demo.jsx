import React from 'react';
import ReactDOM from 'react-dom';
import Dots from './customD3/dataPoints';
import Axis from './customD3/axis';
import Grid from './customD3/grid';
import ToolTip from './customD3/tooltip';

const D3demo = React.createClass({

  propTypes: {
    width:React.PropTypes.number,
    height:React.PropTypes.number,
    chartId:React.PropTypes.string,
    dataUrl:React.PropTypes.string,
  },

  getDefaultProps() {
    return {
      width: 1200,
      height: 300,
      chartId: 'v1_chart',
    };
  },

  getInitialState() {
    return {
      tooltip:{ display:false,data:{key:'',value:''}},
      width:this.props.width,
      data: [],
    };
  },

  componentWillMount(){
    let _self=this;
    window.addEventListener('resize', function(e) {
      _self.updateSize();
    }, true);
    this.setState({width:this.props.width});
  },

  componentDidMount() {
    this.serverRequest = $.get(this.props.dataUrl, function (result) {
      this.setState({
          data: result,
      });
    }.bind(this));
    this.updateSize();
  },

  componentWillUnmount() {
    this.serverRequest.abort();
    window.removeEventListener('resize');
  },

  render(){
    // const data = this.state.data;

    const data = $.extend(true, [], this.state.data);

//     let data = [
//   {"day":"02-11-2016","count":180},
//   {"day":"02-12-2016","count":250},
//   {"day":"02-13-2016","count":150},
//   {"day":"02-14-2016","count":496},
//   {"day":"02-15-2016","count":140},
//   {"day":"02-16-2016","count":380},
//   {"day":"02-17-2016","count":100},
//   {"day":"02-18-2016","count":150},
//   {"day":"02-19-2016","count":250},
//   {"day":"02-20-2016","count":200},
//   {"day":"02-21-2016","count":350},
// ];

    let margin = {top: 5, right: 50, bottom: 20, left: 50},
      w = this.state.width - (margin.left + margin.right),
      h = this.props.height - (margin.top + margin.bottom);

    let parseDate = d3.time.format("%m-%d-%Y").parse;

    data.forEach(function (d) {
      d.date = parseDate(d.day);
    });

    let x = d3.time.scale()
      .domain(d3.extent(data, function (d) {
        return d.date;
      }))
      .rangeRound([0, w]);

    let y = d3.scale.linear()
      .domain([0,d3.max(data,function(d){
        return d.count+100;
      })])
      .range([h, 0]);

    const yAxis = d3.svg.axis()
      .scale(y)
      .orient('left')
      .ticks(5);

    const xAxis = d3.svg.axis()
      .scale(x)
      .orient('bottom')
      .tickValues(data.map(function(d,i){
        if (i>0)
          return d.date;
        }).splice(1))
      .ticks(4);

    const yGrid = d3.svg.axis()
      .scale(y)
      .orient('left')
      .ticks(5)
      .tickSize(-w, 0, 0)
      .tickFormat("");

    let line = d3.svg.line()
      .x(function (d) {
        return x(d.date);
      })
      .y(function (d) {
        return y(d.count);
      }).interpolate('cardinal');

    let transform = 'translate(' + margin.left + ',' + margin.top + ')';

    return (
      <div>
        <h3>Custom D3 Component Chart</h3>
        <svg id={this.props.chartId} width={this.state.width} height={this.props.height}>
          <g transform={transform}>
            <Grid h={h} grid={yGrid} gridType="y"/>
            <Axis h={h} axis={yAxis} axisType="y" />
            <Axis h={h} axis={xAxis} axisType="x"/>
            <path className="line shadow" fill="none" stroke="#3f5175" strokeWidth="5px" d={line(data)} strokeLinecap="round"/>
            <Dots data={data} x={x} y={y} stroke="#ffffff" showToolTip={this.showToolTip} hideToolTip={this.hideToolTip} />
            <ToolTip tooltip={this.state.tooltip}/>
          </g>
        </svg>
      </div>
    );
  },
  updateSize(){
    let node = ReactDOM.findDOMNode(this);
    let parentWidth = node.offsetWidth;
    if (parentWidth < this.props.width) {
      this.setState({width:parentWidth-20});
    } else {
      this.setState({width:this.props.width});
    }
  },
  showToolTip(e){
    e.target.setAttribute('fill', '#FFFFFF');
    this.setState({
      tooltip: {
        display:true,
        data: {
          key:e.target.getAttribute('data-key'),
          value:e.target.getAttribute('data-value'),
        },
        pos:{
          x:e.target.getAttribute('cx'),
          y:e.target.getAttribute('cy'),
        },

      },
    });
  },
  hideToolTip(e){
    e.target.setAttribute('fill', '#7dc7f4');
    this.setState({
      tooltip: {
        display:false,
        data: {
          key:'',
          value:'',
        },
      },
    });
  },
});

export default D3demo;