module.exports = [
  {
    name: "Series 1",
    values: [
      { x: 1, y: 42},
      { x: 2, y: 33},
      { x: 2, y: 4},
      { x: 1, y: 42},
      { x: 4, y: 1},
      { x: 7, y: 4},
      { x: 3, y: 2},
      { x: 5, y: 22},
      { x: 4, y: 10},
      { x: 1, y: 21},
      { x: 4, y: 15},
      { x: 7, y: 43},
      { x: 7, y: 14},
      { x: 10, y: 49},
      { x: 2, y: 32},
      { x: 1, y: 32},
      { x: 6, y: 1},
      { x: 6, y: 49},
      { x: 2, y: 28},
      { x: 8, y: 42},
    ],
  },
  {
    name: "Series 2",
    values: [
      { x: 18, y: 41},
      { x: 18, y: 25},
      { x: 10, y: 38},
      { x: 20, y: 22},
      { x: 10, y: 21},
      { x: 22, y: 3},
      { x: 20, y: 1},
      { x: 20, y: 5},
      { x: 17, y: 29},
      { x: 16, y: 3},
      { x: 17, y: 22},
      { x: 19, y: 41},
      { x: 19, y: 27},
      { x: 19, y: 36},
      { x: 13, y: 41},
      { x: 9, y: 41},
      { x: 24, y: 37},
      { x: 20, y: 6},
      { x: 13, y: 18},
      { x: 20, y: 37},
    ],
  },
  {
    name: "Series 3",
    values: [
      { x: 29, y: 34 },
      { x: 23, y: 29 },
      { x: 39, y: 25 },
      { x: 31, y: 28 },
      { x: 40, y: 41 },
      { x: 33, y: 28 },
      { x: 23, y: 25 },
      { x: 20, y: 34 },
      { x: 21, y: 26 },
      { x: 37, y: 26 },
      { x: 31, y: 39 },
      { x: 28, y: 25 },
      { x: 33, y: 27 },
      { x: 27, y: 34 },
      { x: 36, y: 31 },
      { x: 23, y: 50 },
      { x: 20, y: 36 },
      { x: 20, y: 35 },
      { x: 22, y: 49 },
      { x: 30, y: 35 },
    ],
  },
];
