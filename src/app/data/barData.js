module.exports = [
  {
    name: "Model 1",
    values: [
      { x: "A", y: 40 },
      { x: "B", y: 19 },
      { x: "C", y: 35 },
      { x: "D", y: 8 },
      { x: "E", y: 12 },
    ],
  },
];
