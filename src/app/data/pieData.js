module.exports = [
  {
    label: 'Ford',
    value: 26.0,
  },
  {
    label: 'Chrysler',
    value: 15.0,
  },
  {
    label: 'GMC',
    value: 13.0,
  },
  {
    label: 'Nissan',
    value: 11.0,
  },
  {
    label: 'Kia',
    value: 10.0,
  },
  {
    label: 'Toyota',
    value: 10.0,
  },
  {
    label: 'Audi',
    value: 8.0,
  },
  {
    label: 'BMW',
    value: 7.0,
  },
];
