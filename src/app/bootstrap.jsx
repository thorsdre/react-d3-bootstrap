/**
 * In this file, we create a React component
 * which incorporates components providedby material-ui.
 */

import React from 'react';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';

const Bootstrap = React.createClass({
  render(){
    return(
      <div>
        <h2>Bootstrap React</h2>
        <div className="row">
          <br/>
          <p>Disabled Button</p>
          <ButtonToolbar>
            <Button bsStyle="primary" bsSize="large" disabled>Primary button</Button>
            <Button bsSize="large" disabled>Button</Button>
          </ButtonToolbar>
        </div>
        <div className="row">
          <br/>
          <p>Active Button</p>
          <ButtonToolbar>
            <Button bsStyle="primary" bsSize="large" active>Primary button</Button>
            <Button bsSize="large" active>Button</Button>
          </ButtonToolbar>
        </div>
        <div className="row">
          <br/>
          <p>Default Button</p>
          <ButtonToolbar>
            <Button bsStyle="primary" bsSize="large">Large button</Button>
            <Button bsSize="large">Large button</Button>
          </ButtonToolbar>
          <br/>
          <ButtonToolbar>
            <Button bsStyle="primary">Default button</Button>
            <Button>Default button</Button>
          </ButtonToolbar>
          <br/>
          <ButtonToolbar>
            <Button bsStyle="primary" bsSize="small">Small button</Button>
            <Button bsSize="small">Small button</Button>
          </ButtonToolbar>
          <br/>
          <ButtonToolbar>
            <Button bsStyle="primary" bsSize="xsmall">Extra small button</Button>
            <Button bsSize="xsmall">Extra small button</Button>
          </ButtonToolbar>
        </div>
      </div>
    );
  },
});

export default Bootstrap;